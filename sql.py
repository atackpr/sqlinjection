from flask import Flask, render_template, request
import mysql.connector
import hashlib

app = Flask(__name__)

@app.route('/phone', methods=['GET' , 'POST'])
def pbook():
    if request.method == "POST":
        name = request.form['name']

        try:
            db = mysql.connector.connect(host="localhost",
                                         user="root",
                                         passwd="python1234",
                                         db="sqlinjection")

            cursor = db.cursor()

            query = "select * from phonebook where name = '"
            query += name
            query += "';"
            print query

            count = 0
            login = False
            for result in cursor.execute(query, multi=True):
                if result.with_rows:
                    print("Rows produced by statement '{}':".format(
                        result.statement))
                    ls = result.fetchall()
                    print ls
                    if len(ls) > 0 and count == 0:
                        login = True
                else:
                    print("Number of rows affected by statement '{}': {}".format(
                        result.statement, result.rowcount))

                count += 1

            db.commit()
            cursor.close()
            db.close()

            if login:

                returnString = "<h1>Results:</h1>"

                for item in ls:
                    returnString += '<h3>' + item[0] + " " + item[1] + '</h3>'

                returnString += "<a href='http://localhost:5000/phone'>Back to Phone Book</a>"

                return returnString
            else:
                return "<h1>Person Not Found</h1><a href='http://localhost:5000/phone'>Back to Phone Book</a>"
        except Exception as e:
            strval = str(e)
            msg = "<h1>"
            msg += strval
            msg += "</h1><a href='http://localhost:5000/phone'>Back to Phone Book</a>"
            return msg

    else:
        return render_template('phone.html')


@app.route('/auth', methods=['GET' , 'POST'])
def send():
    if request.method == "POST":
        user = request.form['user']
        pwd = request.form['pwd']
        pwd = hashlib.md5(pwd).hexdigest()


        try:
            db = mysql.connector.connect(host="localhost",
                                         user="root",
                                         passwd="python1234",
                                         db="sqlinjection")

            cursor = db.cursor()

            query = "select * from login where password = '"
            query += pwd
            query += "' and username = '"
            query += user
            query += "';"
            print query

            count = 0
            login = False
            for result in cursor.execute(query, multi=True):
                if result.with_rows:
                    print("Rows produced by statement '{}':".format(
                        result.statement))
                    ls = result.fetchall()
                    print ls
                    if len(ls) > 0 and count == 0:
                        login = True
                else:
                    print("Number of rows affected by statement '{}': {}".format(
                        result.statement, result.rowcount))

                count += 1

            db.commit()
            cursor.close()
            db.close()

            if login:
                return '<h1>Authentication Succesful</h1><h1>Welcome Back, ' + user + "</h1><a href='http://localhost:5000/auth'>Back to Login</a>"
            else:
                return "<h1>Authentication Failed</h1><a href='http://localhost:5000/auth'>Back to Login</a>"
        except Exception as e:
            strval = str(e)
            msg = "<h1>"
            msg += strval
            msg += "</h1><a href='http://localhost:5000/auth'>Back to Login</a>"
            return msg

    else:
        return render_template('index.html')

if __name__ == "__main__":
    app.run(debug=True)